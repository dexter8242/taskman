# TaskMan
TaskMan (short for Task Manager) is a simple yet powerful task manager written in pure Bash. It allows you to create and manage tasks with ease, and provides a clean and intuitive interface that makes it easy to keep track of your progress.

Features:

- Create and manage tasks with ease (Not yet implemented)
- Intuitive interface that makes it easy to keep track of your progress (No TUI yet)
- Powerful filtering and sorting capabilities (No filtering yet, sorry)
- Export tasks to CSV format for further analysis (Not yet implemented)
- Cross-platform compatibility (Linux, Mac, and Windows)

Installation:

    Download the latest release from the GitHub repository.
    Extract the contents of the archive to a directory of your choice.
    Open a terminal and navigate to the directory where you extracted the files.
    Run the following command to install Taskman:

./taskman init

## Usage

To start Taskman, run the following command:

`taskman`

To create a new task, use the "new" command followed by the task name:

`taskman new <task name>`

To edit an existing task, use the "edit" command followed by the task name:

`taskman edit <task name>`

To delete a task, use the "delete" command followed by the task name:

`taskman delete <task name>`

To view a list of all tasks, use the "list" command:

`taskman list`
To filter tasks by status, use the "filter" command followed by the status (e.g., "active", "completed", or "all"):

`taskman filter <status>`

To sort tasks by name, priority, or due date, use the "sort" command followed by the sort criteria (e.g., "name", "priority", or "due"):

`taskman sort <criteria>`

To export tasks to CSV format, use the "export" command followed by the output file name:

`taskman export <output file name>`

To add a comment to a task, use the "comment" command followed by the task name and the comment
